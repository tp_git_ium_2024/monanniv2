# TP2.1: Tester git status
Reprise du TP1 en exécutant un ``git status`` à chaque étape.



## 1-Créer un depot git "MonAnniv" et le cloner :

### Création:

![Q1](Screenshots/TP2 CREATE.png)

### Clonage:

![Q12](Screenshots/TP2 Q1.png)

## 2- Créer un fichier pour lister les invites (un par ligne):

![Q2](Screenshots/TP2 Q2.png)

## 3- Créer un fichier pour les idees de cadeaux (un par ligne)

![Q3](Screenshots/TP2 Q3.png)

## 4- Mise a jour des 2 fichiers dans un meme commit

![Q4](Screenshots/TP2 Q4.png)